# Oganikku


## 1.Goal of project

1.1)make web application that sell vegetables and fruits

## 2.how to start project

2.0)open cmd and use command for install virtual env

    >pip install virtualenv

2.1)clone/pull this respository to your pc

2.2)open cmd and cd to folder that you place this project

2.3)create venv and activate it by use command 

    >virtualenv venv
  
    >venv\Scripts\activate

2.4)install all requriment module by use this command

    >pip install -r requirements.txt
  
2.5)open your vscode

    >code .

2.6)write .env file (config file)

    ```

    FLASK_APP=app.py

    FLASK_ENV=development

    SECRET_KEY=ef10569c-af35-4fec-a21c-820f07402532

    SQLALCHEMY_DATABASE_URI=mysql://root:admin@localhost:3306/db
    
    ```

2.7)run python

    >python app.py
    
### this method you have to already install python 3.7 only**** on your pc and set PATH if not please do it first
In case numpy error for the first time** 
 >python -m pip install numpy==1.19.3 
