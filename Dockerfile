FROM python:3.7 AS builder

RUN apt-get update && apt-get install -y git gcc g++ && apt-get install -y telnet && apt-get install -y iputils-ping && apt-get install -y net-tools
WORKDIR /app
COPY . .

RUN python -m venv .venv && .venv/bin/pip install --no-cache-dir -U pip setuptools
COPY requirements.txt ./

RUN .venv/bin/pip install --no-cache-dir -r requirements.txt
RUN rm -rf /root/.ssh

FROM python:3.7-slim

RUN apt-get update
RUN apt-get install -y tdsodbc unixodbc-dev gcc
RUN apt-get -y install default-libmysqlclient-dev
WORKDIR /app
COPY --from=builder /app /app
ENV PATH="/app/.venv/bin:$PATH"
CMD ["python", "app.py"]