import os
from .. import db
from ..models.user import User, Address
from ..models.shop import Order
from sqlalchemy.exc import IntegrityError
from flask import jsonify, redirect, session, url_for, flash, render_template, current_app
from werkzeug.security import generate_password_hash
from flask_login import login_user, logout_user, current_user
from . import allowed_file

class UserService():


    def __init__(self) -> None:
        pass


    @classmethod
    def register(cls, request):
        email = request.form.get('email')
        username = request.form.get('username')
        password = request.form.get('password')
        confirm_password = request.form.get('confirm_password')
        fullname = request.form.get('fullname')
        if password != confirm_password:
            flash("password was not match")
            print("password was not match")
            return redirect(url_for('user.register'))
        user = User(username, email, generate_password_hash(password), fullname, "default_profile.png", "user")
        try:
            db.session.add(user)
            db.session.commit()
            return redirect(url_for('user.home'))
        except IntegrityError:
            db.session.rollback()
            flash("this email was already use")
            print("this email was already use")
            return redirect(url_for('user.register'))
    
    @classmethod
    def get_profile(cls,user_id):
        user = User.query.filter_by(id=user_id).first()
        return user

        
    @classmethod
    def login(cls, request):
        email = request.form.get('email')
        password = request.form.get('password')
        user = User.query.filter_by(email=email).first()
        if user and user.verify_password(password):
            login_user(user)
            flash("Login Successful")
            return redirect(url_for('shop_list.category_shop_list',item_type="vegetables"))
        else:          
            flash("your password is incorrect")
            return render_template('user/login.html')


    @classmethod
    def update_profile(cls, request):
        email = request.form.get('email')
        username = request.form.get('username')
        fullname = request.form.get('fullname')
        user = User.query.filter_by(email=email).first()
        try:
            user.username = username
            user.fullname = fullname
            db.session.commit()
            print("Update your profile successful")
            flash("Update your profile successful")
            return redirect(url_for('user.profile'))
        except Exception as e: 
            print(e)
            print("Cannot update your profile")
            flash("Cannot update your profile")
            return redirect(url_for('user.profile'))


    @classmethod
    def update_profile_image(cls, request):
        # check if the post request has the file part
        if 'file' not in request.files:
            print('No file part')
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            print('No selected file')
        if file and allowed_file(file.filename):
            filename = current_user.get_id()+"_profile."+file.filename.rsplit('.', 1)[1].lower()
            if os.path.exists(current_app.config['UPLOAD_FOLDER']+"/user_profile/"+filename):
                os.remove(current_app.config['UPLOAD_FOLDER']+"/user_profile/"+filename)
            file.save(os.path.join(current_app.config['UPLOAD_FOLDER']+"/user_profile/", filename))

        user = User.query.filter_by(id=current_user.get_id()).first()
        try:
            user.profile_image = filename
            db.session.commit()
            print("Update your profile successful")
            flash("Update your profile successful")
            return redirect(url_for('user.profile'))
        except Exception as e: 
            print(e)
            print("Cannot update your profile")
            flash("Cannot update your profile")
            return redirect(url_for('user.profile'))


    @classmethod
    def add_address(cls, request):
        name = request.form.get('name')
        address_form = request.form.get('address')
        country = request.form.get('country')
        postcode = str(request.form.get('postcode'))
        phone = request.form.get('phone')
        address = Address(name,address_form,country,postcode,int(current_user.get_id()),phone)
        print(postcode)
        try:
            db.session.add(address)
            db.session.commit()
            return redirect(url_for('shop_list.confirm_order'))
        except IntegrityError:
            db.session.rollback()
            flash("error occur")
            print("error occur")
            return redirect(url_for('user.add_address'))


    @classmethod
    def logout(cls):
        session.clear()
        logout_user()
        flash("logout Successful")
        return redirect(url_for('user.home'))