
from .. import db
from ..models.user import User
from ..models.shop import Item,Order
from flask import redirect, url_for, flash, render_template, session, current_app
from . import allowed_file
import os
from sqlalchemy.exc import IntegrityError

class AdminService():

    def __init__(self) -> None:
        pass


    @classmethod
    def add_new_product(cls, request):
        name = request.form.get('name')
        price = request.form.get('price')
        item_type = request.form.get('item_type')
        # check if the post request has the file part
        if 'image_preview' not in request.files:
            print('No file image_preview')
        if 'image' not in request.files:
            print('No file image')
        image = request.files['image']
        image_preview = request.files['image_preview']
        # If the user does not select a file, the browser submits an
        if image and allowed_file(image.filename):
            image_filename = name+"_"+item_type+"."+image.filename.rsplit('.', 1)[1].lower()
            if os.path.exists(current_app.config['UPLOAD_FOLDER']+"/product_bg/"+image_filename):
                os.remove(current_app.config['UPLOAD_FOLDER']+"/product_bg/"+image_filename)
            image.save(os.path.join(current_app.config['UPLOAD_FOLDER']+"/product_bg/", image_filename))
        else:
            print("cannot save image")
            flash("cannot save image")
            return redirect(url_for("shop_list.category_shop_list",item_type=item_type))
        if image_preview and allowed_file(image_preview.filename):
            image_preview_filename = name+"_"+item_type+"_preview."+image_preview.filename.rsplit('.', 1)[1].lower()
            if os.path.exists(current_app.config['UPLOAD_FOLDER']+"/preview_product/"+image_preview_filename):
                os.remove(current_app.config['UPLOAD_FOLDER']+"/preview_product/"+image_preview_filename)
            image_preview.save(os.path.join(current_app.config['UPLOAD_FOLDER']+"/preview_product/", image_preview_filename))
        else:
            print("cannot save preview_image")
            flash("cannot save preview_image")
            return redirect(url_for("shop_list.category_shop_list",item_type=item_type))
        item = Item(name, price, item_type, image_filename, image_preview_filename)
        try:
            db.session.add(item)
            db.session.commit()
            return redirect(url_for("shop_list.category_shop_list",item_type=item_type))
        except IntegrityError:
            db.session.rollback()
            return redirect(url_for("shop_list.category_shop_list",item_type=item_type))


    @classmethod
    def get_chat_list(cls):
        all_user = User.query.filter(User.role != "admin").all()
        return render_template("admin/admin_chat.html",all_user=all_user)


    @classmethod
    def get_all_orders(cls):
        order = Order.query.all()
        return render_template('admin/order.html', orders=order)

    
    @classmethod
    def update_delivery_status(cls,request):
        delivery_status = request.form.get('delivery_status')
        tran_code = request.form.get('tran_code')
        order_id = request.form.get('order_id')
        tran_code = None if tran_code == 'None' or tran_code == '' else tran_code
        order = Order.query.filter_by(id=order_id).first()
        try:
            order.tran_code = tran_code
            order.status = delivery_status
            db.session.commit()
            print("Update order successful")
            flash("Update order successful")
            return redirect(url_for('admin.order_list'))
        except Exception as e: 
            print(e)
            print("Cannot update order")
            flash("Cannot update order")
            return redirect(url_for('admin.order_list'))