from datetime import datetime
import os

from .. import db
from ..models.shop import Item, OrderItem, Order, TranOption, PaymentMethod
from ..models.user import Address

from flask import redirect, url_for, flash, render_template, session
from flask_login import current_user


class ShopListService():

    def __init__(self) -> None:
        pass
    

    @classmethod
    def get_by_category(cls, item_type):
        items = Item.query.filter_by(item_type=item_type).all()
        return render_template('shop/category_item_list.html', items=items,item_type=item_type.capitalize())


    @classmethod
    def get_by_id(cls, item_id):
        item = Item.query.filter_by(id=item_id).first()
        return render_template("shop/product.html",item=item)


    @classmethod
    def add_to_cart(cls, request):
        name = request.form.get('name')
        id = request.form.get('id')
        amount = int(request.form.get('amount'))
        pp = float(request.form.get('pp'))
        preview_image = request.form.get('preview_image')
        new_item = {"amount":amount,"pp":pp, "preview_image":preview_image, "name":name}
        if 'cart' in session:
            cart = session['cart']
            if id in cart.keys():
                cart[id]["amount"] += amount
            else:
                cart[id] = new_item
            session["total_cost"] += pp*amount
            session['cart'] = cart
        else:
            session['cart'] = {id:new_item}
            session['total_cost'] = pp*amount
        return redirect(url_for('shop_list.category_shop_list',item_type="vegetables"))
    
    @classmethod
    def update_cart(cls, request):
        id = request.form.get('id')
        amount = int(request.form.get('amount'))
        if 'cart' in session:
            cart = session['cart']
            if id in cart.keys():
                dif = amount-cart[id]["amount"]
                session['total_cost'] += cart[id]["pp"]*dif
                cart[id]["amount"] = amount
            session['cart'] = cart
        return redirect(url_for('shop_list.category_shop_list',item_type="vegetables"))


    @classmethod
    def get_confirmation(cls):
        del_list = []
        cart = session['cart']
        for item in cart.keys():
            if cart[item]["amount"] == 0:
                del_list.append(item)
        for item in del_list:
            del cart[item]
        session['cart'] = cart
        address = Address.query.filter_by(user_id=current_user.get_id()).all()
        tran_option = TranOption.query.all()
        return render_template('shop/confirm_order.html', cart=session['cart'] , count=len(list(session['cart'].keys())),address=address,tran_option=tran_option)


    @classmethod
    def get_payment(cls):
        payment = PaymentMethod.query.all()
        delivery = TranOption.query.filter_by(id=session['shipping']["delivery"]).first()
        return render_template('shop/payment.html', cart=session['cart'] , count=len(list(session['cart'].keys())),payment=payment,total_cost=session["total_cost"],delivery_price=delivery.get_data()["price"])


    @classmethod
    def confirm_order(cls, request):
        address_id = int(request.form.get('address_id'))
        delivery = int(request.form.get('delivery_id'))
        session['shipping'] = {"address_id":address_id,"delivery":delivery}
        print(session['shipping'])
        return redirect(url_for('shop_list.payment'))


    @classmethod
    def purchase(cls, request):
        shipping = session['shipping']
        payment_method = int(request.form.get('payment_method'))
        tran = TranOption.query.filter_by(id=shipping["delivery"]).first()
        if 'cart' in session:
            cart = session['cart']
            order = Order(user_id=current_user.get_id(), 
            payment_method_id=payment_method,
            tran_option_id=shipping["delivery"],
            address_id=shipping["address_id"],
            date = datetime.now().strftime("%d-%m-%Y, %H:%M:%S"),
            total= int(session["total_cost"])+int(tran.get_data()["price"]))
            for item in cart.keys():
                new_item = OrderItem(item,cart[item]["amount"])
                order.items.append(new_item)
                db.session.add(new_item)
        try:
            db.session.add(order)
            db.session.commit()
            session.pop('cart', None)
            session.pop('total_cost', None)
            session.pop('shipping', None)
            return redirect(url_for('shop_list.order_success'))
        except Exception as e:
            print(e)
            return redirect(url_for('shop_list.confirm_order'))


    @classmethod
    def get_order_list(cls):
        order = Order.query.filter_by(user_id=current_user.get_id()).all()
        return render_template('shop/bill.html',orders=order)
    

    @classmethod
    def get_delivery_status(cls):
        order = Order.query.filter_by(user_id=current_user.get_id()).all()
        return render_template('shop/delivery.html',orders=order)
    


   