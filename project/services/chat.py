from datetime import datetime
from .. import db
from ..models.user import Message, User
from flask import render_template
class ChatService():

    def __init__(self) -> None:
        pass


    @classmethod
    def save_message(cls, message, sender, user_id):
        date = datetime.now().strftime("%d-%m-%Y, %H:%M:%S"),
        msg_obj = Message(message, sender, user_id, date)
        try:
            db.session.add(msg_obj)
            db.session.commit()
        except Exception as e:
            print(e)

    
    @classmethod
    def get_user_message(cls, user_id):
        return Message.query.filter_by(user_id=user_id).all()
    