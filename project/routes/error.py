from flask import Blueprint,request, render_template, redirect, url_for, session

error = Blueprint('error', __name__)

@error.route('/no_permission')
def no_permission():
    return render_template("error_handeler/not_admin.html")