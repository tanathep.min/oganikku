from functools import wraps
from flask_login import current_user
from flask import Blueprint,request, redirect, url_for, session
from project.services.admin import AdminService
admin = Blueprint('admin', __name__)

def is_admin(view):
    @wraps(view)
    def wrapped_view(**kwargs):
        if current_user.is_authenticated:
            if current_user.is_admin() == False:
                return redirect(url_for('error.no_permission'))
            else:
                return view(**kwargs)
        else:
            return redirect(url_for('user.login'))
    return wrapped_view

#for admin get all user in database
@admin.route('/chat')
@is_admin
def admin_chat():
    return AdminService.get_chat_list()

#for admin to add new product
@admin.route('/add_new_product', methods=["POST"])
@is_admin
def add_new_product():
    return AdminService.add_new_product(request)

#for admin toget all order page
@admin.route('/order', methods=["GET"])
@is_admin
def order_list():
    return AdminService.get_all_orders()

#for admin to update order data like status or tran_code
@admin.route('/order/update', methods=["POST"])
@is_admin
def update_order():
    return AdminService.update_delivery_status(request)