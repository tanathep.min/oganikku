from flask_login import login_required, current_user

from flask import Blueprint,request, render_template, session, redirect, url_for
from ..services.shop_list import ShopListService
from ..services.chat import ChatService

shop_list = Blueprint('shop_list', __name__)

#get item by categories ['vegetables','fruits','seasonal']
@shop_list.route('/<item_type>', methods=["GET"])
@login_required
def category_shop_list(item_type):
    if item_type in ['vegetables','fruits','seasonal']:
        return ShopListService.get_by_category(item_type)
    
#to get basket page with your item
@shop_list.route('/basket', methods=["GET"])
@login_required
def basket():
    try:
        return render_template('shop/basket.html', cart=session['cart'], total_cost=session['total_cost'])
    except:
        return render_template('shop/basket.html', cart={}, total_cost=0)

#for update your item in basket(replace)
@shop_list.route('/basket/update', methods=["POST"])
@login_required
def update_basket():
    return ShopListService.update_cart(request)

#confirm your basket and clear if you cancle some item
@shop_list.route('/basket/confirm', methods=["GET","POST"])
@login_required
def confirm_order():
    if request.method == "GET":
        try:
            return ShopListService.get_confirmation()
        except Exception as e: 
            print(e)
            return redirect(url_for('shop_list.category_shop_list',item_type="vegetables"))
    else:
        return ShopListService.confirm_order(request)


@shop_list.route('/complete', methods=["GET"])
@login_required
def order_success():
    return render_template('complete.html')



@shop_list.route('/address/form', methods=["GET"])
@login_required
def shipaddressform():
    return render_template('shop/shipping-form.html')

#purchase after select payment method
@shop_list.route('/payment', methods=["GET","POST"])
@login_required
def payment():
    if request.method == 'POST':
        return ShopListService.purchase(request)
    else:
        return ShopListService.get_payment()

#get all user order
@shop_list.route('/bill', methods=["GET"])
@login_required
def bill():
    return ShopListService.get_order_list()

#get all delivery status
@shop_list.route('/delivery', methods=["GET"])
@login_required
def deli():
    return ShopListService.get_delivery_status()

#view 1 item as full page
@shop_list.route('/<item_type>/<int:item_id>', methods=["GET"])
@login_required
def get_item_by_id(item_type,item_id):
    return ShopListService.get_by_id(item_id)

#add item to basket
@shop_list.route('/add_to_cart', methods=["POST"])
@login_required
def add_to_cart():
    return ShopListService.add_to_cart(request)