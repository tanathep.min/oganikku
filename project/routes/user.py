from flask import Blueprint,request, render_template, redirect, url_for, session, current_app
from flask_login import login_required, current_user
from ..services.user import UserService

import os

# @socketio.on('my event')
# def handle_my_custom_event(json):
#     print('received json: ' + str(json))

user = Blueprint('user', __name__)

@user.route('/')
def home():
    return render_template("home.html")


@user.route('/register', methods=["GET","POST"])
def register():
    if request.method == 'POST':
        return UserService.register(request)
    elif request.method == 'GET':
        if current_user.is_authenticated:
            return redirect(url_for('user.home'))
        else:
            return render_template("user/register.html")


@user.route('/login', methods=["GET","POST"])
def login():
    if request.method == 'POST':
        return UserService.login(request)
    elif request.method == 'GET':
        if current_user.is_authenticated:
            return redirect(url_for('user.home'))
        else:
            return render_template("user/login.html")


@user.route('/profile', methods=["GET"])
@login_required
def profile():
    if request.method == 'GET':
        return render_template("user/profile.html")


@user.route('/update_profile', methods=["POST"])
@login_required
def update_profile():
    return UserService.update_profile(request)

@user.route('/update_profile/image', methods=["POST"])
@login_required
def upload_image_profile():
    if request.method == 'POST':
        return UserService.update_profile_image(request)


@user.route('/address/add', methods=["POST"])
@login_required
def add_address():
    if request.method == 'POST':
        return UserService.add_address(request)


@user.route('/logout', methods=["POST"])
@login_required
def logout():
    if request.method == 'POST':
        return UserService.logout()