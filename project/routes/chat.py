from .. import socketio
from flask_socketio import emit, join_room, leave_room
from ..services.chat import ChatService
from ..services.user import UserService
from flask import Blueprint,redirect,url_for,render_template
import json
from sqlalchemy.ext.declarative import DeclarativeMeta
from flask_login import login_required,current_user

#encode class object to json serialization
class AlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data) # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields

        return json.JSONEncoder.default(self, obj)

chat = Blueprint('chat', __name__)

#get chat data of 1 user with admin as json
@chat.route('/get_message/<int:user_id>', methods=['POST'])
def get_user_message(user_id):
    return json.dumps({"message":ChatService.get_user_message(user_id),
            "user":UserService.get_profile(user_id)}, cls=AlchemyEncoder)

#chat for normal user
@chat.route('/chat', methods=["GET"])
@login_required
def chat_to_admin():
    if current_user.is_authenticated:
        if current_user.is_admin():
            return redirect(url_for('admin.admin_chat'))
    all_message = ChatService.get_user_message(current_user.get_id())
    return render_template('user/message.html', all_message=all_message)


@socketio.on('join_room')
def handle_join_room_event(data):
    room_id = "1&"+str(data['customer'])
    join_room(room_id)


@socketio.on('leave_room')
def on_leave(data):
    room_id = "1&"+str(data['customer'])
    leave_room(room_id)


#get message from admin and send to user
@socketio.on('recive_from_admin')
def recive_from_admin(data):
    ChatService.save_message(data["msg"], "admin", data['customer'])
    socketio.emit('admin_response',{"msg":data["msg"],"name":data["name"],"profile_image":data["profile_image"]}, room="1&"+str(data['customer']))


#get message from user and send to admin
@socketio.on('recive_from_user')
def recive_from_user(data):
    print("receive")
    ChatService.save_message(data["msg"], "user", data['customer'])
    socketio.emit('user_response',{"msg":data["msg"],"name":data["name"],"profile_image":data["profile_image"]}, room="1&"+str(data['customer']))