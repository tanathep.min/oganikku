import os
from flask import Flask, redirect, url_for, request
from flask_sqlalchemy import SQLAlchemy
from .config import Config
from flask_login import LoginManager
from werkzeug.security import generate_password_hash
from flask_socketio import SocketIO



# init SQLAlchemy so we can use it later in our models
db = SQLAlchemy()
socketio = SocketIO()



def create_app():
    app = Flask(__name__)
    
    app.config.from_object(Config)

    # migrate database settings
    db.init_app(app)

    #demo data and create database table
    import project.models as models
    with app.app_context():
        db.create_all()
        try:
            admin = models.user.User("admin", "admin@mail.com", generate_password_hash("admin1234"), "admin", "default_profile.png", "admin")
            kbank = models.shop.PaymentMethod(name="KBANK", image="kasikorn.png")
            paypal = models.shop.PaymentMethod(name="PAYPAY", image="paypal.png")
            visa = models.shop.PaymentMethod(name="VISACARD", image="visa.png")
            free_deli = models.shop.TranOption(desc="Free delivery - Order over 1000 THB and Live in BKK", price=0)
            inter_over = models.shop.TranOption(desc="Inter Express (delivery in other provinces ) - Order over", price=190)
            inver_less = models.shop.TranOption(desc="Inter Express (delivery in other provinces ) - Order lesser", price=320)
            ems = models.shop.TranOption(desc="Thai post office (ems) weighing by weight", price=200)
            db.session.add(admin)
            db.session.add(kbank)
            db.session.add(paypal)
            db.session.add(visa)
            db.session.add(free_deli)
            db.session.add(inter_over)
            db.session.add(inver_less)
            db.session.add(ems)
            db.session.commit()
        except Exception as e:
            pass

    login_manager = LoginManager() # Login manager for flask-login # New
    login_manager.init_app(app)

    
    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return models.user.User.query.get(int(user_id))

    #handle if user didnt login
    @login_manager.unauthorized_handler
    def handle_needs_login():
        return redirect(url_for('user.login', next=request.endpoint))

    # blueprint for parts of app
    import project.routes as routes
    app.register_blueprint(routes.user)
    app.register_blueprint(routes.chat)
    app.register_blueprint(routes.admin , url_prefix='/admin')
    app.register_blueprint(routes.error)
    app.register_blueprint(routes.shop_list, url_prefix='/shop')

    socketio.init_app(app)
    
    return (app,socketio)

    