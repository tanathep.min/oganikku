from .. import db
from flask_login import UserMixin
from werkzeug.security import check_password_hash

class User(UserMixin, db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(200))
    fullname = db.Column(db.String(100))
    profile_image = db.Column(db.String(50))
    role = db.Column(db.String(10))
    addresses = db.relationship('Address', backref='user', lazy="dynamic")
    orders = db.relationship('Order', backref='user', lazy="dynamic")

    def __init__(self, username, email, password, fullname, profile_image, role):
        self.username = username
        self.email = email
        self.password = password
        self.fullname = fullname
        self.profile_image = profile_image
        self.role = role

    def __repr__(self):
        return '<User %r, Email %r>' % (self.username, self.email)
    
    def is_admin(self):
        if self.role == 'admin':
            return True
        return False

    def get_profile(self):
        return {
            "username" : self.username,
            "email" : self.email,
            "fullname" : self.fullname,
            "profile_image" : self.profile_image,
            "role" : self.role,
        }

    def verify_password(self, pwd):    
        return check_password_hash(self.password, pwd)


class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    address = db.Column(db.String(500), nullable=False)
    country = db.Column(db.String(500), nullable=False)
    postcode = db.Column(db.String(20), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    phone = db.Column(db.String(10))

    def __init__(self, name, address, country, postcode, user_id, phone):
        self.name = name
        self.address = address
        self.country = country
        self.postcode = postcode
        self.user_id = user_id
        self.phone = phone


class Message(db.Model):
    __tablename__ = 'message'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    message = db.Column(db.String(500), nullable=False)
    sender = db.Column(db.String(30), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    time = db.Column(db.String(50))
    user = db.relationship('User', backref='messages')

    def __init__(self, message, sender, user_id, time):
        self.message = message
        self.sender = sender
        self.user_id = user_id
        self.time = time