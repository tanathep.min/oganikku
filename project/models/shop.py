from .. import db

class Item(db.Model):
    __tablename__ = 'item'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(80), unique=True)
    price = db.Column(db.Float)
    item_type = db.Column(db.String(100))
    image = db.Column(db.String(80))
    preview_image = db.Column(db.String(80))

    def __init__(self, name, price, item_type, image, preview_image):
        self.name = name
        self.price = price
        self.item_type = item_type
        self.image = image
        self.preview_image = preview_image

    def __repr__(self):
        return '<Item : %r, Price: %f/KG, item_type: %r>' % (self.name,self.price,self.item_type)


class Order(db.Model):
    __tablename__ = 'order'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    tran_code = db.Column(db.String(100))
    status = db.Column(db.String(50), nullable=False)
    payment_method_id = db.Column(db.Integer, db.ForeignKey('payment_method.id'), nullable=False)
    tran_option_id = db.Column(db.Integer, db.ForeignKey('tran_option.id'), nullable=False)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=False)
    total = db.Column(db.Float, nullable=False)
    date = db.Column(db.String(50))
    payment_method = db.relationship('PaymentMethod', backref='order')
    tran_option = db.relationship('TranOption', backref='order')
    address = db.relationship('Address', backref='order')
    items = db.relationship('OrderItem', lazy='dynamic', backref='order')

    def __init__(self, user_id, payment_method_id, tran_option_id, address_id, total, date, tran_code=None, status="packing"):
        self.user_id = user_id
        self.tran_code = tran_code
        self.payment_method_id = payment_method_id
        self.status = status
        self.tran_option_id = tran_option_id
        self.address_id = address_id
        self.total = total
        self.date = date

    def __repr__(self):
        return '<New order for : %r, payment_method: %r, address : %r>' % (self.user_id, self.payment_method, self.address)


class OrderItem(db.Model):
    __tablename__ = 'order_item'
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'), primary_key=True)
    item_id = db.Column(db.Integer,db.ForeignKey('item.id'), primary_key=True)
    amount = db.Column(db.Integer, nullable=False)
    # item = db.relationship('Item', lazy='dynamic', backref='order_item')

    def __init__(self, item_id, amount):
        self.item_id = item_id
        self.amount = amount


    # def __repr__(self):
    #     return '<Item : %r, Price: %f/KG, item_type: %r>' % (self.name,self.price,self.item_type)

class TranOption(db.Model):
    __tablename__ = 'tran_option'
    id = db.Column(db.Integer, primary_key=True,  autoincrement=True)
    desc = db.Column(db.String(300), nullable=False)
    price = db.Column(db.Integer, nullable=False)

    def __init__(self, desc, price):
        self.desc = desc or None
        self.price = price
    
    def get_data(self):
        return {
            "desc": self.desc, 
            "price": self.price
        }


class PaymentMethod(db.Model):
    __tablename__ = 'payment_method'
    id = db.Column(db.Integer, primary_key=True,  autoincrement=True)
    name = db.Column(db.String(50), primary_key=True, nullable=False)
    desc = db.Column(db.String(300))
    image = db.Column(db.String(100), nullable=False)

    def __init__(self, name, image, desc=None):
        self.name = name
        self.desc = desc
        self.image = image
